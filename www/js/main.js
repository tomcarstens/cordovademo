function drawLine(points, map) {
  var path = new google.maps.Polyline({
    path : points,
    geodesic: true,
    strokeColor: '#000000',
    strokeOpacity: 1.0,
    strokeWeight: 1
  });
  path.setMap(map);
  return path;
}

function main() {
  var mapOptions = {
    center: { lat: 44.9833, lng: -93.2667},
    zoom: 15
  };
  var map = new google.maps.Map(document.getElementById('map-canvas'),  mapOptions);
  var watchid = null;
  var start = document.createElement('div');
  var btn = document.createElement('button');
  btn.innerText = "Start Route";
  btn.onclick = function () {
    watchid = navigator.geolocation.watchPosition(function (position) {
      //track location
      var latlng =  new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      map.setCenter(latlng)
      path.getPath().push(latlng);
    });
  };
  start.appendChild(btn);
  var stopbutton = document.createElement('button');
  stopbutton.innerText = "Stop Route";
  stopbutton.onclick = function () {
    navigator.geolocation.clearWatch(watchid);
  };
  start.appendChild(stopbutton);
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(start);
  path = drawLine([], map);
}
google.maps.event.addDomListener(window, 'load', main);
